//
//  ClsUserDetails.swift
//  PerfectDemo
//
//  Created by Dhruvit on 19/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage

class ClsUserDetails: NSObject,NSCoding {

    var strId : String! = EMPTYSTRING
    var strEmail : String! = EMPTYSTRING
    var strFirstName : String! = EMPTYSTRING
    var strLastName : String! = EMPTYSTRING
    var strGender : String! = EMPTYSTRING
    var strContactNo : String! = EMPTYSTRING
    var strProfilePicture : String! = EMPTYSTRING
    var strLatitude : String! = EMPTYSTRING
    var strLongitude : String! = EMPTYSTRING
    var strAccessToken : String! = EMPTYSTRING
    var imgProfilePic : UIImage!
    
    let imageManager = SDWebImageManager.shared()
    
    override init() {
        //        init empty values
    }
    
    init(pdictResponse : [String:Any]) {
        self.strId = pdictResponse.getValueIfAvilable(key: KID) as? String
        self.strEmail = pdictResponse.getValueIfAvilable(key: KEMAIL) as? String
        self.strFirstName = pdictResponse.getValueIfAvilable(key: KFBFIRSTNAME) as? String
        self.strLastName = pdictResponse.getValueIfAvilable(key: KFBLASTNAME) as? String
        self.strGender = pdictResponse.getValueIfAvilable(key: KGENDER) as? String
        self.strContactNo = pdictResponse.getValueIfAvilable(key: KCONTACTNO) as? String
        self.strProfilePicture = pdictResponse.getValueIfAvilable(key: KPROFILEPICTURE) as? String
        self.strLatitude = pdictResponse.getValueIfAvilable(key: KLATITUDE) as? String
        self.strLongitude = pdictResponse.getValueIfAvilable(key: KLONGITUDE) as? String
        self.strAccessToken = pdictResponse.getValueIfAvilable(key: KACCESSTOKEN) as? String
        
    }
    
    func downloadProfilePic() {
        
        let strImagageUrl = self.strProfilePicture
        
        imageManager.imageDownloader?.downloadImage(with: URL(string: strImagageUrl!), options: SDWebImageDownloaderOptions.continueInBackground, progress: nil, completed: { (image, data, error, success) in
            
            if (image != nil) && (success == true) {
                self.imgProfilePic = image
                let imgData = UIImageJPEGRepresentation(image!, 1.0)
                USERDEFAULTS.set(imgData, forKey: KPROFILEPICTURE)
            }
        })
    }
    
    required init(coder aDecoder : NSCoder) {
        self.strId = aDecoder.decodeObject(forKey: KID) as? String
        self.strEmail = aDecoder.decodeObject(forKey: KEMAIL) as? String
        self.strFirstName = aDecoder.decodeObject(forKey: KFBFIRSTNAME) as? String
        self.strLastName = aDecoder.decodeObject(forKey: KFBLASTNAME) as? String
        self.strGender = aDecoder.decodeObject(forKey: KGENDER) as? String
        self.strContactNo = aDecoder.decodeObject(forKey: KCONTACTNO) as? String
        self.strProfilePicture = aDecoder.decodeObject(forKey: KPROFILEPICTURE) as? String
        self.strLatitude = aDecoder.decodeObject(forKey: KLATITUDE) as? String
        self.strLongitude = aDecoder.decodeObject(forKey: KLONGITUDE) as? String
        self.strAccessToken = aDecoder.decodeObject(forKey: KACCESSTOKEN) as? String
    }
    
    func  encode(with _aCoder : NSCoder) {
        _aCoder.encode(self.strId , forKey: KID)
        _aCoder.encode(self.strEmail, forKey: KEMAIL)
        _aCoder.encode(self.strFirstName , forKey: KFBFIRSTNAME)
        _aCoder.encode(self.strLastName , forKey: KFBLASTNAME)
        _aCoder.encode(self.strGender , forKey: KGENDER)
        _aCoder.encode(self.strContactNo , forKey: KCONTACTNO)
        _aCoder.encode(self.strProfilePicture , forKey: KPROFILEPICTURE)
        _aCoder.encode(self.strLatitude , forKey: KLATITUDE)
        _aCoder.encode(self.strLongitude , forKey: KLONGITUDE)
        _aCoder.encode(self.strAccessToken , forKey: KACCESSTOKEN)
    }
    
}
