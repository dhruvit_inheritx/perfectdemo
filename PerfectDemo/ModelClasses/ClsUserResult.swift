//
//  ClsUserResult.swift
//  PerfectDemo
//
//  Created by Dhruvit on 29/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit

class ClsUserResult: NSObject {
    
    var strUserID : String! = EMPTYSTRING
    var strID1 : String! = EMPTYSTRING
    var strID2 : String! = EMPTYSTRING
    var strTotalLike1 : String! = EMPTYSTRING
    var strTotalLike2 : String! = EMPTYSTRING
    var strUserImage1 : String! = EMPTYSTRING
    var strUserImage2 : String! = EMPTYSTRING
    
    override init() {
        //        init empty values
    }
    
    init(pdictResponse : [String:Any]) {
        self.strUserID = pdictResponse.getValueIfAvilable(key: KUSERID) as? String
        self.strID1 = pdictResponse.getValueIfAvilable(key: KID1) as? String
        self.strID2 = pdictResponse.getValueIfAvilable(key: KID2) as? String
        self.strTotalLike1 = pdictResponse.getValueIfAvilable(key: KTOTALLIKE1) as? String
        self.strTotalLike2 = pdictResponse.getValueIfAvilable(key: KTOTALLIKE2) as? String
        self.strUserImage1 = pdictResponse.getValueIfAvilable(key: KUSERIMAGE1) as? String
        self.strUserImage2 = pdictResponse.getValueIfAvilable(key: KUSERIMAGE2) as? String
    }
    
}
