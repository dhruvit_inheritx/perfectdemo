//
//  ProfileView.swift
//  PerfectDemo
//
//  Created by Kuldeep Gadhvi on 18/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class ProfileView: UIViewController ,UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    @IBOutlet var imgSelected : UIImageView!
    
    @IBOutlet weak var txtFirstName : UITextField?
    @IBOutlet weak var txtLastName : UITextField?
    @IBOutlet weak var txtEmail : UITextField?
    @IBOutlet weak var txtPhone : UITextField?
    @IBOutlet weak var btnMale : UIButton?
    @IBOutlet weak var btnFemale : UIButton?
    
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
//    @IBOutlet weak var btnProfilePic : UIButton?
    
    var Gendar = "male"
    var isImageSelected : Bool = false
    
    var objUserDetails : ClsUserDetails!
    let imageManager = SDWebImageManager.shared()
    
    override func viewDidLoad() {
//        USERDEFAULTS.getUserDetailsObject().strProfilePicture
//        imgSelected.image =UIImage()
        
//        USERDEFAULTS.getUserDetailsObject().strProfilePicture
        
        if let userDetail = USERDEFAULTS.getUserDetailsObject() as ClsUserDetails? {
            objUserDetails = userDetail
        }
        
        if let strFirstName = objUserDetails.strFirstName {
            txtFirstName?.text = strFirstName
        }
        
        if let strLastName = objUserDetails.strLastName {
            txtLastName?.text = strLastName
        }

        if let strEmail = objUserDetails.strEmail {
            txtEmail?.text = strEmail
        }

        if let strContactNo = objUserDetails.strContactNo as String? {
            
            if strContactNo != "" {
                if strContactNo.characters.count >= 10 {
                    
                    let num: String = Helper.formatNumber(mobileNumber: strContactNo)
                    let str1 = num[0..<2]
                    let str2 = num[3..<5]
                    let str3 = num[6..<9]
                    
                    txtPhone?.text = "("+str1+") "+str2+"-"+str3
                }
                else {
                    txtPhone?.text = strContactNo
                }
            }
            else {
                txtPhone?.text = strContactNo
            }
        }
        
        if let strGendar = objUserDetails.strGender {
            
            if strGendar == "female" {
                Gendar = "female"
                btnFemale?.setImage(UIImage(named : "circle"), for: UIControlState.normal)
                btnMale?.setImage(UIImage(named : "circle1"), for: UIControlState.normal)
            }else{
                Gendar = "male"
                btnFemale?.setImage(UIImage(named : "circle1"), for: UIControlState.normal)
                btnMale?.setImage(UIImage(named : "circle"), for: UIControlState.normal)
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            self.imgSelected?.layer.cornerRadius = (self.imgSelected?.frame.size.height)!/2
            
        }
        self.loadUserImage()
        // Do any additional setup after loading the view.
    }
    
    func loadUserImage() {
        
//        imgSelected.sd_setImage(with: URL(string : USERDEFAULTS.getUserDetailsObject().strProfilePicture), placeholderImage: UIImage(named:"profile-round"))
        
        activityIndicator.startAnimating()
        
        let strImagageUrl = USERDEFAULTS.getUserDetailsObject().strProfilePicture
        
        if let imgData = USERDEFAULTS.value(forKey: KPROFILEPICTURE) as! Data? {
            let image = UIImage(data: imgData)
            self.imgSelected.image = image
            activityIndicator.stopAnimating()
        }
        else {
            imageManager.imageDownloader?.downloadImage(with: URL(string: strImagageUrl!), options: SDWebImageDownloaderOptions.continueInBackground, progress: nil, completed: { (image, data, error, success) in
                
                if (image != nil) && (success == true) {
                    self.imgSelected.image = image
                    self.activityIndicator.stopAnimating()
                    let imgData = UIImageJPEGRepresentation(image!, 1.0)
                    USERDEFAULTS.set(imgData, forKey: KPROFILEPICTURE)
                }
                else {
                    self.activityIndicator.stopAnimating()
                }
            })
        }
        
    }
    
    @IBAction func btnProfilePicClicked(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "Select Image From", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imgPicker = UIImagePickerController()
                imgPicker.sourceType = .photoLibrary
                imgPicker.delegate = self
                imgPicker.allowsEditing = true
                DispatchQueue.main.async {
                    self.present(imgPicker, animated: true, completion: nil)
                }
            }
            else {
                Helper.showAlertBar(alertBarType: AlertBarType.Warning, message: "Photo Library not Available.")
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imgPicker = UIImagePickerController()
                imgPicker.sourceType = .camera
                imgPicker.delegate = self
                imgPicker.allowsEditing = true
                DispatchQueue.main.async {
                    self.present(imgPicker, animated: true, completion: nil)
                }
            }
            else {
                Helper.showAlertBar(alertBarType: AlertBarType.Warning, message: "Camera not Available.")
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnGendar(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if sender.tag == 1 {
            Gendar = "female"
            btnFemale?.setImage(UIImage(named : "circle"), for: UIControlState.normal)
            btnMale?.setImage(UIImage(named : "circle1"), for: UIControlState.normal)


        }
        else {
            Gendar = "male"
            
            btnFemale?.setImage(UIImage(named : "circle1"), for: UIControlState.normal)
            btnMale?.setImage(UIImage(named : "circle"), for: UIControlState.normal)

        }
        
    }
    
//    @IBAction func handleTap1(sender: UITapGestureRecognizer) {
//        
//        let alert = UIAlertController(title: "Select Image From", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
//        
//        alert.addAction(UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.default, handler: { (action) in
//            
//            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
//                let imgPicker = UIImagePickerController()
//                imgPicker.sourceType = .photoLibrary
//                imgPicker.delegate = self
//                imgPicker.allowsEditing = true
//                DispatchQueue.main.async {
//                    self.present(imgPicker, animated: true, completion: nil)
//                }
//            }
//            else {
//                Helper.showAlertBar(alertBarType: AlertBarType.Warning, message: "Photo Library not Available.")
//            }
//            
//        }))
//        
//        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (action) in
//            
//            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
//                let imgPicker = UIImagePickerController()
//                imgPicker.sourceType = .camera
//                imgPicker.delegate = self
//                imgPicker.allowsEditing = true
//                DispatchQueue.main.async {
//                    self.present(imgPicker, animated: true, completion: nil)
//                }
//            }
//            else {
//                Helper.showAlertBar(alertBarType: AlertBarType.Warning, message: "Camera not Available.")
//            }
//            
//        }))
//        
//        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
//        
//        
//        self.present(alert, animated: true, completion: nil)
//    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let selectedImage = info[UIImagePickerControllerEditedImage] as! UIImage
        imgSelected.image = selectedImage
        isImageSelected = true
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func validateUserData() {
        
        self.view.endEditing(true)
        
        let strPhone = Helper.formatNumber(mobileNumber: (txtPhone?.text)!)
        
        objUserDetails = USERDEFAULTS.getUserDetailsObject()
        
        if txtFirstName?.text == objUserDetails.strFirstName && txtLastName?.text == objUserDetails.strLastName && txtEmail?.text == objUserDetails.strEmail && strPhone == objUserDetails.strContactNo && Gendar == objUserDetails.strGender && isImageSelected == false
        {
            return
        }
        
        if txtFirstName?.text?.isEmptyString() == true {
            Helper.showAlertBar(alertBarType: AlertBarType.Error, message: "Please Valid First Name.")
        }
        else if txtLastName?.text?.isEmptyString() == true {
            Helper.showAlertBar(alertBarType: AlertBarType.Error, message: "Please enter Last Name.")
        }
        else if txtEmail?.text?.isEmptyString() == false && txtEmail?.text?.isValidEmail() == false {
            Helper.showAlertBar(alertBarType: AlertBarType.Error, message: "Please enter Valid Email Address.")
        }
        else if strPhone.isEmptyString() == false && strPhone.isValidPhoneNumber() == false {
            Helper.showAlertBar(alertBarType: AlertBarType.Error, message: "Please enter Valid Phone Number.")
        }
        else if strPhone.isEmptyString() == false && strPhone.length != 10 {
            Helper.showAlertBar(alertBarType: AlertBarType.Error, message: "Phone Number must be 10 digit long.")
        }
        else {
            self.saveUserProfile()
        }
    }
    
    func saveUserProfile() {
        
        if Helper.checkInternetConnection() == false {
            
            Helper.openErrorVC(strTitle: "Profile", isNetworkError: true, parentViewContoller: self, complitionBlock: { (success) in
                DispatchQueue.main.async(execute: {
                    self.saveUserProfile()
                })
            })
            
            return
        }
        
        var dictUpload = [String:Data]()
        
        if isImageSelected == true && imgSelected.image != nil {
            let imgData = UIImageJPEGRepresentation(imgSelected.image!, 2.0)
            dictUpload[KPROFILEPICTURE] = imgData
        }
        
        let strPhone = Helper.formatNumber(mobileNumber: (txtPhone?.text)!)
        
        var dictParam = [String:Any]()
        dictParam[KEMAIL] = txtEmail?.text
        dictParam[KFBFIRSTNAME] = txtFirstName?.text
        dictParam[KFBLASTNAME] = txtLastName?.text
        dictParam[KGENDER] = Gendar
        dictParam[KCONTACTNO] = strPhone
        
        APICallManager.sharedInstance.callUploadWebServiceWithParameter(uploadData : dictUpload ,parameters: dictParam, urlPath: ApiCallPath.editProfile.rawValue, isBackgroundCall: false, pAlertView: APPDELEGATE.window, pViewController: self) { (apiResult, response) in
            
            if apiResult == ApiResult.success {
                let result = response as! [String : Any]
                print("result :- ",result)
                
                Helper.saveUserDetails(dictDetail: result)
                Helper.showAlertBar(alertBarType: AlertBarType.Success, message: "Profile Saved Successfully.")
            }
            else if apiResult == ApiResult.error && response != nil {
                print("response :- ",response as! String)
                Helper.hideHud(self.view)
                
                Helper.showAlertBar(alertBarType: AlertBarType.Error, message: response as! String)
            }
            else {
                Helper.openErrorVC(strTitle: "Profile", isNetworkError: false, parentViewContoller: self, complitionBlock: { (success) in
                    DispatchQueue.main.async(execute: {
                        self.validateUserData()
                    })
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ProfileView : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        
        let newLength = currentCharacterCount + string.characters.count - range.length
        
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        
        if textField == txtPhone {
            
            let length: Int = Int(Helper.getLength(mobileNumber: textField.text!))
            if length == 10 {
                if range.length == 0 {
                    return false
                }
            }
            if length == 3 {
                let num: String = Helper.formatNumber(mobileNumber: textField.text!)
                textField.text = "(\(num)) "
                if range.length > 0 {
                    textField.text = "\(num[0..<2])"
                }
            }
            else if length == 6 {
                let num: String = Helper.formatNumber(mobileNumber: textField.text!)
                textField.text = "(\(num[0..<2])) \(num[3..<5])-"
                if range.length > 0 {
                    textField.text = "(\(num[0..<2])) \(num[3..<5])"
                }
            }
            return newLength <= 14
        }
        else if textField == txtEmail {
            return newLength <= 30
        }
        else {
            return newLength <= 30
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
}
