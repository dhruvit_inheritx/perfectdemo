//
//  SettingView.swift
//  PerfectDemo
//
//  Created by Kuldeep Gadhvi on 10/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit

class SettingView: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var SelectedSetting: ((_ success : String) -> Void)?

    var ArrTable = ["Profile","Terms and Conditions","Privacy Policy","Contact Us","Version","Logout"]
    @IBOutlet weak var tblView : UITableView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView?.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrTable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = ArrTable[indexPath.row]
        cell?.imageView?.image = UIImage(named: String(describing: indexPath.row+1))
        print("ok")
        cell?.textLabel?.textColor = UIColor.init(colorLiteralRed: 135/255.0, green: 135/255.0, blue: 135/255.0, alpha: 1)
        cell?.textLabel?.font = UIFont(name: "Titillium-Regular", size: 12)
        
        cell?.accessoryType = .disclosureIndicator
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.row == 5) {
            Helper.showAlert("Are you sure want to logout?", cancelButtonTitle: "CANCEL", otherButtonTitle: "OK", pobjView: APPDELEGATE.window, completionBock: { (customAlert, buttonIndex) in
                
                if buttonIndex == 1 {
                    self.logout()
                }
            })
        }
        else {
            if let callback = self.SelectedSetting {
                callback (ArrTable[indexPath.row])
                self.dismiss(animated: false)
            }
            
            if indexPath.row == 0 {
                let MyView = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView")
                self.navigationController?.pushViewController(MyView!, animated: true)
            }
            else {
                let MyView = self.storyboard?.instantiateViewController(withIdentifier: "CommonView") as! CommonView
                MyView.index = indexPath.row
                self.navigationController?.pushViewController(MyView, animated: true)
            }
        }
    }
    
    func logout() {
        
        if Helper.checkInternetConnection() == false {
            
            Helper.openErrorVC(strTitle: "Settings", isNetworkError: true, parentViewContoller: self, complitionBlock: { (success) in
                DispatchQueue.main.async(execute: {
                    self.logout()
                })
            })
            
            return
        }
        
        APICallManager.sharedInstance.callWebServiceWithParameter(parameters: [:], urlPath: ApiCallPath.logout.rawValue, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (apiResult, response) in
            
            if apiResult == ApiResult.success {
                APPDELEGATE.Logout()
            }
            else if apiResult == ApiResult.error && response != nil {
                Helper.showAlertBar(alertBarType: AlertBarType.Error, message: response as! String)
            }
            else {
                Helper.openErrorVC(strTitle: "Settings", isNetworkError: false, parentViewContoller: self, complitionBlock: { (success) in
                    DispatchQueue.main.async(execute: {
                        self.logout()
                    })
                })
            }
        }
    }

}
