//
//  LoginViewController.swift
//  PerfectDemo
//
//  Created by Dhruvit on 02/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var lblFacebook : FXLabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        lblFacebook?.setGradientColor()
    }
    
    @IBAction func logInWithFacebook(sender : UIButton) {
        
        if Helper.checkInternetConnection() == false {
            
            AlertBar.show(type: AlertBarType.Error, message: "No Internet Connection.", parentView: self.view)
            return
        }
        
        APICallManager.sharedInstance.signInWithFacebook(parentViewController: self) { (apiResult, response) in
            
            print("login view")
            if apiResult == ApiResult.success{
                Helper.hideHud((self.view)!)
                let result = response as! [String : Any]
                Helper.saveUserDetails(dictDetail: result)
                Helper.showHud(self.view)
                self.showHomeScreen()
            }
            else if apiResult == ApiResult.error && response != nil {
                print("response :- ",response as! String)
                Helper.hideHud(self.view)
                Helper.showAlertBar(alertBarType: AlertBarType.Error, message: response as! String, parentView: self.view)
            }
            else {
                Helper.hideHud(self.view)
            }
        }
    }
    
    func showHomeScreen() -> Void {
        
        
        
        DispatchQueue.main.async {
            
            Helper.showHud(APPDELEGATE.window!)
            
            Helper.addToUserDefaults(pobject: true as AnyObject, forKey: kISUSERLOGGEDIN)
            
            UIApplication.shared.isStatusBarHidden = true
            let objStoryboard: UIStoryboard = UIStoryboard(name: sMain, bundle: Bundle.main)
            let objVC: UIViewController = objStoryboard.instantiateInitialViewController()!
            APPDELEGATE.window!.rootViewController = objVC
            APPDELEGATE.window!.makeKeyAndVisible()
            //        self.performSegue(withIdentifier: cSegueMainStoryboard, sender: nil)
        }
    }
    
}
