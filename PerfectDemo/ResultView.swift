//
//  ResultView.swift
//  PerfectDemo
//
//  Created by Kuldeep Gadhvi on 11/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit
import SDWebImage

class ResultView: UIViewController {

    @IBOutlet var imgView1 : UIImageView!
    @IBOutlet var imgView2 : UIImageView!
    
    var objUserResult : ClsUserResult!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func manageSubViews() {
        
        if objUserResult != nil {
            imgView1.sd_setImage(with: URL(string: objUserResult.strUserImage1))
            imgView2.sd_setImage(with: URL(string: objUserResult.strUserImage2))
        }
        
    }
    
    func displayResult() {
        
        if Helper.checkInternetConnection() == false {
            
            Helper.openErrorVC(strTitle: "Result", isNetworkError: true, parentViewContoller: self, complitionBlock: { (success) in
                DispatchQueue.main.async(execute: {
                    self.displayResult()
                })
            })
            
            return
        }
        
        APICallManager.sharedInstance.callWebServiceWithParameter(parameters: [:], urlPath: ApiCallPath.displayResult.rawValue, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (apiResult, response) in
            
            if apiResult == ApiResult.success {
                let result = response as! [String : Any]
                print("response :- ",result)
                
                Helper.hideHud(self.view)
                
                let temp = result[KUSERRESULT] as! [Any]
                
                if temp.count != 0 {
                    print("temp.count :- ",temp.count)
                    self.objUserResult = ClsUserResult(pdictResponse: result[KUSERRESULT] as! [String : Any])
                    
                    Helper.showAlertBar(alertBarType: AlertBarType.Success, message: "Display Successfully.")
                    self.manageSubViews()
                }
                
            }
            else if apiResult == ApiResult.error && response != nil {
                Helper.showAlertBar(alertBarType: AlertBarType.Error, message: response as! String)
            }
            else {
                Helper.openErrorVC(strTitle: "Result", isNetworkError: false, parentViewContoller: self, complitionBlock: { (success) in
                    DispatchQueue.main.async(execute: {
                        self.displayResult()
                    })
                })
            }
        }
    }
    
    func finishResult() {
        
        if Helper.checkInternetConnection() == false {
            
            Helper.openErrorVC(strTitle: "Result", isNetworkError: true, parentViewContoller: self, complitionBlock: { (success) in
                DispatchQueue.main.async(execute: {
                    self.finishResult()
                })
            })
            
            return
        }
        
        APICallManager.sharedInstance.callWebServiceWithParameter(parameters: [:], urlPath: ApiCallPath.finished.rawValue, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (apiResult, response) in
            
            if apiResult == ApiResult.success {
                Helper.showAlertBar(alertBarType: AlertBarType.Success, message: "Finished Successfully.")
            }
            else if apiResult == ApiResult.error && response != nil {
                Helper.showAlertBar(alertBarType: AlertBarType.Error, message: response as! String)
            }
            else {
                Helper.openErrorVC(strTitle: "Result", isNetworkError: false, parentViewContoller: self, complitionBlock: { (success) in
                    DispatchQueue.main.async(execute: {
                        self.finishResult()
                    })
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
