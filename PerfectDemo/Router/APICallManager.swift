//
//  APICallManager.swift
//  PerfectDemo
//
//  Created by Dhruvit on 04/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit
import SystemConfiguration
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit

typealias APICallCompletionBlock = (ApiResult,Any?) -> ()

enum ApiResult : Int {
    case success = 1
    case error = 2
    case failure = 3
}

enum ApiCallPath : String {
    case login = "login"
    case userProfile = "user-profile"
    case updateLatLong = "update-user-lat-long"
    case editProfile = "edit-profile"
    case userList = "user-list"
    case displayResult = "display-result"
    case imageAction = "image-act"
    case imageUpload = "images-upload"
    case logout = "logout"
    case finished = "finished"
}

enum ImageAction : String {
    case like = "1"
    case abuse = "2"
    case nude = "3"
    case notForApp = "4"
}

enum APICallRouter : URLRequestConvertible {
    
    static let baseURLString = KBASEURL
    
    static var Latitude: String?
    static var Longitude: String?
    static var AccessToken: String?
    static var UserID: String?
    
    case loginUser([String: Any])
    case getUserProfile([String:Any])
    case updateUserLatLong([String:Any])
    case editProfile([String:Any])
    case getUserList([String:Any])
    case displayResult([String:Any])
    case imageAction([String:Any])
    case uploadImage([String:Any])
    
    
    var method : HTTPMethod {
        switch self {
        case .loginUser:
            return .post
        case .getUserProfile:
            return .post
        case .updateUserLatLong:
            return .post
        case .editProfile:
            return .post
        case .getUserList:
            return .post
        case .displayResult:
            return .post
        case .imageAction:
            return .post
        case .uploadImage:
            return .post
        }
    }
    
    var path : String {
        switch self {
        case .loginUser:
            return "login"
        case .getUserProfile:
            return "user-profile"
        case .updateUserLatLong:
            return "update-user-lat-long"
        case .editProfile:
            return "edit-profile"
        case .getUserList:
            return"user-list"
        case .displayResult:
            return "display-result"
        case .imageAction:
            return "image-act"
        case .uploadImage:
            return "images-upload"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try APICallRouter.baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .getUserProfile(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .loginUser(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .updateUserLatLong(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .editProfile(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .getUserList(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .displayResult(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .imageAction(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .uploadImage(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
    
}

class APICallManager: NSObject {
    
    static let sharedInstance : APICallManager = {
        let instance = APICallManager()
        return instance
    }()
    
    func callWebServiceWithParameter(parameters : [String:Any],urlPath : String, isBackgroundCall : Bool, pAlertView : UIView?, pViewController : UIViewController?,pCompletionBlock : @escaping APICallCompletionBlock ) {
        
        
        if isBackgroundCall == false {
            Helper.showHud(pAlertView!)
        }
        
        var dictParam = parameters
        dictParam = addOtherParameters(dictData: dictParam)
        
        let strPath = APICallRouter.baseURLString+urlPath
        
        Alamofire.request(strPath, method: .post, parameters: dictParam, encoding: URLEncoding.default).responseJSON(options: .allowFragments, completionHandler: { (response) -> Void in
            
            print("Url :- ",strPath)
            print("Parameters :- ",dictParam)
            
            APICallManager.sharedInstance.parseResponse(response: response, isBackgroundCall : isBackgroundCall, pAlertView: pAlertView, pViewController: pViewController, pCompletionBlock: { (success, result) -> () in
                pCompletionBlock(success, result)
            })
        })
        
    }
    
    func callUploadWebServiceWithParameter(uploadData : [String:Data]? = nil,parameters : [String:Any],urlPath : String, isBackgroundCall : Bool, pAlertView : UIView?, pViewController : UIViewController?,pCompletionBlock : @escaping APICallCompletionBlock ) {
        
        if isBackgroundCall == false {
            Helper.showHud(pAlertView!)
        }
        
        var dictParam = parameters
        dictParam = addOtherParameters(dictData: dictParam)
        
        print("Param:",dictParam)
        
        let strPath = APICallRouter.baseURLString+urlPath
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                for (key, value) in dictParam {
                    let str = String(describing: value)
                    let strData = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
                    multipartFormData.append(strData!, withName: key)
                }
                
                if uploadData != nil {
                    for (key, value) in uploadData! {
                        multipartFormData.append(value, withName: key, fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                    }
                }
                
        },
            to: strPath,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let uploadRequest, _, _):
                    
                    let request = uploadRequest.request!
                    
                    print("Request URL : ", request.url!)
                    
                    if request.httpMethod == "POST" || request.httpMethod == "post" {
                        print("Request Param : " + self.getParameterString(URLRequest: request as! NSMutableURLRequest))
                    }
                    else if request.httpMethod == "PUT" || request.httpMethod == "put" {
                        print("Request Param : " + self.getParameterString(URLRequest: request as! NSMutableURLRequest))
                    }
                    
                    uploadRequest.responseJSON { response in
                        
                        APICallManager.sharedInstance.parseResponse(response: response, isBackgroundCall : isBackgroundCall, pAlertView: pAlertView, pViewController: pViewController, pCompletionBlock: { (apiResult, response) -> () in
                            pCompletionBlock(apiResult, response)
                        })
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    pCompletionBlock(ApiResult.failure,"Something Went Wrong.")
                }
        }
        )
    }
    
    func addOtherParameters(dictData : [String:Any]) -> [String:Any] {
        
        var dictTemp = dictData
        
        if let accessToken = USERDEFAULTS.object(forKey: KACCESSTOKEN) {
            APICallRouter.AccessToken  = accessToken as? String
            dictTemp[KACCESSTOKEN] = accessToken
        }
        
        if let userID = USERDEFAULTS.object(forKey: KID) {
            APICallRouter.UserID  = userID as? String
            dictTemp[KID] = userID
        }
        
        if let latitude = USERDEFAULTS.object(forKey: KLATITUDE) {
            APICallRouter.Latitude  = latitude as? String
            dictTemp[KLATITUDE] = latitude
        }
        else {
            dictTemp[KLATITUDE] = "00"
        }
        
        if let longitude = USERDEFAULTS.object(forKey: KLONGITUDE) {
            APICallRouter.Longitude  = longitude as? String
            dictTemp[KLONGITUDE] = longitude
        }
        else {
            dictTemp[KLONGITUDE] = "00"
        }
        
        return dictTemp
    }
    
    func callWebService(URLRequestConvert : URLRequestConvertible, isBackgroundCall : Bool, pAlertView : UIView?, pViewController : UIViewController?,pCompletionBlock : @escaping APICallCompletionBlock ) {
        
        if isBackgroundCall == false {
            Helper.showHud(pAlertView!)
        }
        
        do {
            var urlRequest = try URLRequestConvert.asURLRequest()
            
            print("Request URL : ", urlRequest.url!)
            
            if urlRequest.httpMethod == "POST" || urlRequest.httpMethod == "post" {
                print("Request Param : " + self.getParameterString(URLRequest: urlRequest as! NSMutableURLRequest))
            }
            else if urlRequest.httpMethod == "PUT" || urlRequest.httpMethod == "put" {
                print("Request Param : " + self.getParameterString(URLRequest: urlRequest as! NSMutableURLRequest))
            }
        }
        catch {
            print("Dim background error")
        }
        
        Alamofire.request(URLRequestConvert).responseJSON(options: .allowFragments, completionHandler: { (response) -> Void in
            APICallManager.sharedInstance.parseResponse(response: response, isBackgroundCall : isBackgroundCall, pAlertView: pAlertView, pViewController: pViewController, pCompletionBlock: { (success, result) -> () in
                pCompletionBlock(success, result)
            })
        })
    }
    
    func parseResponse(response : DataResponse<Any>, isBackgroundCall : Bool, pAlertView : UIView?, pViewController : UIViewController?, pCompletionBlock:APICallCompletionBlock){
        
        switch response.result {
        case .success(let JSON):
            let dicResponse = JSON as! [String : Any]
            print("dicResponse with JSON: \(dicResponse)")
            
            if isBackgroundCall == false {
                Helper.hideHud(pAlertView!)
            }
            
            let errorMessage = dicResponse[KERROR]  as! String
            let strMessage = dicResponse[KMESSAGE]  as! String
            let dictJsonData = dicResponse[KDATA] as? [String:Any]
            
            if ((errorMessage) == "-2") || ((errorMessage) == "-1") {
                pCompletionBlock(ApiResult.error,strMessage)
//                Helper.hideHud((pViewController?.view)!)

                APPDELEGATE.Logout()
            }
            
            if (dictJsonData != nil && dictJsonData!.count > 0) {
//                Helper.hideHud(pAlertView!)
                pCompletionBlock(ApiResult.success,dictJsonData)
            }
            else if ((errorMessage) != "0") {
//                Helper.hideHud((pViewController?.view)!)

                pCompletionBlock(ApiResult.error,strMessage)
            }
            else if ((errorMessage) == "0") {
//                Helper.hideHud((pViewController?.view)!)

                pCompletionBlock(ApiResult.success,strMessage)
            }
            
//            Helper.hideHud(pAlertView!)
            
        case .failure(let error):
            print("Request failed with error: \(error)")
            
            if isBackgroundCall == false {
                Helper.hideHud(pAlertView!)
            }
//            Helper.hideHud((pViewController?.view)!) 
            pCompletionBlock(ApiResult.failure,error.localizedDescription as AnyObject?)
        }
    }
    
    func logoutFromFacebook(){
        APPDELEGATE.Logout()
    }
    
    func signInWithFacebook(parentViewController : UIViewController ,pCompletionBlock : @escaping APICallCompletionBlock) {
        
        let fbManager = FBSDKLoginManager()
        fbManager.logOut()
        Helper.showHud(parentViewController.view)
        
        fbManager.logIn(withReadPermissions: ["public_profile","email"], from: parentViewController, handler: { (result, error) in
            
            if error != nil{
                print(error!)
                Helper.hideHud(parentViewController.view)
                pCompletionBlock(ApiResult.failure,error?.localizedDescription)
                
            }
            else if (result?.isCancelled)!{
                print("Cancelled")
                Helper.hideHud(parentViewController.view)
                pCompletionBlock(ApiResult.error,"Login cancelled." as Any?)
            }
            else {
                Helper.hideHud(parentViewController.view)
                self.getFacebookUserDetails(parentViewController: parentViewController, pCompletionBlock: { (apiResult, response) in
                    
                    pCompletionBlock(apiResult, response)
                })
            }
        })
    }
    
    func getFacebookUserDetails(parentViewController : UIViewController, pCompletionBlock : @escaping APICallCompletionBlock) {
        
        if((FBSDKAccessToken.current()) != nil) {
            
            Helper.showHud(parentViewController.view)
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id,name,email,picture.type(large), last_name,first_name,gender,locale,age_range"]).start { (connection, result, error) in
                
                if (error != nil){
                    Helper.hideHud(parentViewController.view)
                    pCompletionBlock(ApiResult.error,error?.localizedDescription as AnyObject?)
                    print("\(String(describing: error?.localizedDescription))")

                    return
                }
                else if result != nil {
                    Helper.hideHud(parentViewController.view)
                    
                    let dicResult = result as! [String : AnyObject?]
                    
                    //                    pCompletionBlock(true,dicResult)
                    
                    let strUserId = dicResult[KID] as! String
                    
                    var userDetails = [String : Any]()
                    userDetails[KFBID] = strUserId as AnyObject?
                    
                    if USERDEFAULTS.value(forKey: KDEVICETOKEN) != nil{
                        userDetails[KDEVICETOKEN] = USERDEFAULTS.value(forKey: KDEVICETOKEN) as AnyObject?
                    }
                    else {
                        userDetails[KDEVICETOKEN] = "123456789" as AnyObject?
                    }
                    
                    let strEmail:String? = dicResult[KEMAIL] as? String
                    let strFirstName:String? = dicResult[KFBFIRSTNAME] as? String
                    let strLastName:String? = dicResult[KFBLASTNAME] as? String
                    let strGender:String? = dicResult[KGENDER] as? String
                    
                    let dictPicture = dicResult[KFBPICTURE] as? [String:AnyObject]
                    let dictData = dictPicture?[KDATA] as? [String:AnyObject]
                    let strUrl:String? = dictData?[KFBURL] as? String
                    
                    userDetails[KEMAIL] = strEmail as AnyObject?
                    userDetails[KFBFIRSTNAME] = strFirstName! as AnyObject?
                    userDetails[KFBLASTNAME] = strLastName as AnyObject?
                    userDetails[KGENDER] = strGender as AnyObject?
                    userDetails[KPROFILEPICTURE] = strUrl as AnyObject?
                    
                    userDetails = self.addOtherParameters(dictData: userDetails)
                    
                    //                    print(userDetails)
                    
                    APICallManager.sharedInstance.callWebService(URLRequestConvert: APICallRouter.loginUser(userDetails), isBackgroundCall : false, pAlertView : parentViewController.view, pViewController: parentViewController, pCompletionBlock: { (apiResult, response) -> () in
                        pCompletionBlock(apiResult,response)
                    })
                }
                else {
                    pCompletionBlock(ApiResult.failure, "Something went wrong." as Any?)
                    Helper.hideHud(parentViewController.view)
                }
            }
        }
    }
    
    func isConnectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func getParameterString(URLRequest: NSMutableURLRequest) -> String {
        if URLRequest.httpBody != nil {
            return String(data: URLRequest.httpBody!, encoding:String.Encoding.utf8)!
        }
        return ""
    }
    
}
