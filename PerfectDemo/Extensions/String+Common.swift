//
//  String+Common.swift
//  PerfectDemo
//
//  Created by Dhruvit on 05/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import Foundation

let INTFIRST : Int = 1
let STREMAILPREDICATE = "SELF MATCHES %@"

extension String {
    
    fileprivate func stringFromResult(_ result: UnsafeMutablePointer<CUnsignedChar>, length: Int) -> String {
        let hash = NSMutableString()
        for i in 0..<length {
            hash.appendFormat("%02x", result[i])
        }
        return String(hash)
    }
    
    var length: Int {
        return characters.count
    }
    
    var first: String {
        return String(characters.prefix(INTFIRST))
    }
    
    var last: String {
        return String(characters.suffix(INTFIRST))
    }
    
    var uppercaseFirst: String {
        return first.uppercased() + String(characters.dropFirst())
    }
    
    func getDate(_ pstrFormat:String) -> Date{
        
        let dateString = self
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = pstrFormat
        let dateFromString = dateFormatter.date(from: dateString)
        return dateFromString!
    }
    
    func getLocalDate(_ pstrFormat : String) -> Date {
        
        let dateString = self
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = pstrFormat
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let date = dateFormatter.date(from: dateString)
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = pstrFormat
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        let stringFromDate = dateFormatter.string(from: date!)
        let dateFromString = dateFormatter.date(from: stringFromDate)
        
        return dateFromString!
    }
    
    func isValidEmail() -> Bool {
        let emailRegex: String = EMAILREGEX
        let emailTest: NSPredicate = NSPredicate(format: STREMAILPREDICATE, emailRegex)
        return emailTest.evaluate(with: self)
    }
    
    
    func isEmptyString() -> Bool {
        let aStrTrimName: String = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as String
        if aStrTrimName.length == INTZERO {
            return true
        }
        return false
    }
    
    func isEqualToIgnoringCase(_ strToCompare:String) -> Bool {
        var strToCompare = strToCompare
        strToCompare = strToCompare.lowercased()
        let originalString = self.lowercased() as String
        if originalString as String == strToCompare {
            return true
        }
        return false
    }
    
    func isValidName() -> Bool {
        let RegEx = "\\A\\w\\z"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: self)
    }
    
    func isValidPhoneNumber() -> Bool {
        
        let charcter  = CharacterSet(charactersIn: PHONECHARS).inverted
        var filtered:String!
        let inputString = self.components(separatedBy: charcter)
        filtered = inputString.joined(separator: STREMPTY)
        return  self == filtered
    }
    
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
    
    func removeSpecialChars(_ strException : String) -> String {
        let strSet = "1234567890" + strException
        let validChars : Set<Character> = Set(strSet.characters)
        return String(self.characters.filter {validChars.contains($0)})
    }
    
    mutating func removeLastCharIfExist(_ char : Character){
        while self.characters.last == char{
            if self.characters.last == char{
                self = String(self.characters.dropLast())
            }
        }
    }
    
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
    
    // for convenience we should include String return
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = self.index(self.startIndex, offsetBy: r.lowerBound)
        let end = self.index(self.startIndex, offsetBy: r.upperBound)
        
        return self[start...end]
    }
}
