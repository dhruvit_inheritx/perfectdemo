//
//  UserDefaults+Common.swift
//  PerfectDemo
//
//  Created by Dhruvit on 19/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    func setUserDetailsObject(_ pObject:ClsUserDetails) {
        let data : Data = NSKeyedArchiver.archivedData(withRootObject: pObject)
        USERDEFAULTS.set(data, forKey: KUSERDETAILS)
        USERDEFAULTS.synchronize()
    }
    
    func getUserDetailsObject()-> ClsUserDetails{
        let tmpObject : ClsUserDetails =  ClsUserDetails()
        if let objUserData = USERDEFAULTS.object(forKey: KUSERDETAILS) as? Data {
            if let userSelectedObject = NSKeyedUnarchiver.unarchiveObject(with: objUserData) as? ClsUserDetails {
                return userSelectedObject
            }
        }
        return tmpObject
    }
    
}
