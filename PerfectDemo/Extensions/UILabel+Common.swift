//
//  UILabel+Common.swift
//  PerfectDemo
//
//  Created by Kuldeep Gadhvi on 17/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import Foundation

extension FXLabel {
    
    func setGradientColor() {
        self.gradientStartColor = UIColor.init(colorLiteralRed: 120/255.0, green: 187/255.0, blue: 106/255.0, alpha: 1)
        self.gradientEndColor = UIColor.init(colorLiteralRed: 75/255.0, green: 177/255.0, blue: 173/255.0, alpha: 1)
        self.gradientStartPoint = CGPoint(x: 0.0, y: 0.0)
        self.gradientEndPoint = CGPoint(x: 0.0, y: 0.7)
    }
    
}
