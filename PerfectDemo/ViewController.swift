
//
//  ViewController.swift
//  Perfect Attire
//
//  Created by Kuldeep Gadhvi on 20/04/17.
//  Copyright © 2017 Kuldeep Gadhvi. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class ViewController: UIViewController,UIScrollViewDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var ScrView : UIScrollView!
    @IBOutlet weak var ScrMain : UIScrollView!
    
    var arrayImage1 = [UIImage]()
    var arrayImage2 = [UIImage]()
    var arrayDict = [[String:String]]()
    var arrayTemp = [[String:String]]()
    
    @IBOutlet weak var img1 : UIImageView!
    @IBOutlet weak var img2 : UIImageView!

    @IBOutlet weak var imgBack1 : UIImageView!
    @IBOutlet weak var imgBack2 : UIImageView!

    @IBOutlet weak var imgBlur1 : UIVisualEffectView!
    @IBOutlet weak var imgBlur2 : UIVisualEffectView!
    
    @IBOutlet weak var btnUser : UIButton?
    @IBOutlet weak var btnReport : UIButton?
    @IBOutlet weak var btnEdit : UIButton?
    @IBOutlet weak var btnBack : UIButton?
    
    var Nav2 : UINavigationController?
    var Nav1 : UINavigationController?
    
    @IBOutlet weak var lblHeader : FXLabel!
    
    @IBOutlet weak var PageView : UIView?
    @IBOutlet weak var PageRound1 : UIView?
    @IBOutlet weak var PageRound2 : UIView?
    
    @IBOutlet weak var imgNoPost : UIImageView?
    //    @IBOutlet weak var lblNoImage : UILabel?
    @IBOutlet weak var lblDescription : UILabel?
    
    @IBOutlet weak var btnBottom1 : UIButton?
    @IBOutlet weak var btnBottom2 : UIButton?
    @IBOutlet weak var btnBottom3 : UIButton?
    
    //    var ImageCounter = 0
    //    var IsImage = false
    var ImageNumber = "1"
    var IsLoading =  false
    var not_user_ids = ""
    
    var isLoadimgImage = false
    
    let imageManager = SDWebImageManager.shared()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SettingsView" {
            Nav2 = segue.destination as? UINavigationController;
            let Setting = (Nav2?.viewControllers[0]) as! SettingView
            Setting.SelectedSetting = {
                select in
                
                self.lblHeader.text = select
                
                self.btnBack?.isHidden = false
                
                if select == "Profile" {
                    self.lblHeader.text = "Edit Profile"
                    self.btnEdit?.isHidden = false;
                }
                else {
                    self.btnEdit?.isHidden = true;
                }
            }
        }
        else if segue.identifier == "Upload" {
            Nav1 = segue.destination as? UINavigationController;
            let Upload = (Nav1?.viewControllers[0]) as! UploadView
            Upload.ImageUploaded = {
                select in
                self.ScrMain.contentOffset = CGPoint(x: self.view.frame.size.width, y: 0)
                
                if self.arrayDict.count == 0 {
                    self.getUserList()
                }
                
                APPDELEGATE.isUpload = false
                
                if APPDELEGATE.isUpload == false {
                    self.btnBottom1?.setTitle("Result", for: UIControlState.normal)
                    self.lblHeader.text = "Perfect Attire"
                }
            }
        }
        else {
            
        }
    }
    
    func loadUserImage() {
        
        let strImagageUrl = USERDEFAULTS.getUserDetailsObject().strProfilePicture
        
        if let imgData = USERDEFAULTS.value(forKey: KPROFILEPICTURE) as! Data? {
            let image = UIImage(data: imgData)
            self.btnUser?.setBackgroundImage(image, for: .normal)
        }
        else {
            imageManager.imageDownloader?.downloadImage(with: URL(string: strImagageUrl!), options: SDWebImageDownloaderOptions.continueInBackground, progress: nil, completed: { (image, data, error, success) in
                
                if (image != nil) {
                    self.btnUser?.setBackgroundImage(image, for: .normal)
                    let imgData = UIImageJPEGRepresentation(image!, 1.0)
                    USERDEFAULTS.set(imgData, forKey: KPROFILEPICTURE)
                }
            })
        }
    }
    
    override func viewDidLoad() {
        
        self.ScrMain.isHidden = true
        self.ScrView.isHidden = true
        
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.ScrMain.contentOffset = CGPoint(x: self.view.frame.size.width, y: 0)
            self.ScrMain.contentSize = CGSize(width: self.ScrMain.frame.size.width*3, height: self.ScrMain.frame.size.height)
            self.ScrView.contentSize = CGSize(width: self.ScrView.frame.size.width*2, height: self.ScrView.frame.size.height)
            self.ScrMain.isHidden = false
            self.ScrView.isHidden = false
        }
        
        Helper.hideHud(APPDELEGATE.window!)
        
        APPDELEGATE.checkLocationPermission()
        
        btnBack?.isHidden = true
        
        self.loadUserImage()
        self.getUserList()
        
        lblDescription?.text = "Description"
        
        btnUser?.layer.borderColor = UIColor.clear.cgColor
        btnUser?.layer.borderWidth = 0.5
        btnUser?.layer.cornerRadius = (btnUser?.frame.size.height)!/2
        
        PageView?.layer.cornerRadius = (PageView?.frame.size.height)!/2
        PageRound1?.layer.cornerRadius = (PageRound1?.frame.size.height)!/2
        PageRound2?.layer.cornerRadius = (PageRound2?.frame.size.height)!/2
        
        //        lblNoImage?.isHidden = true
        imgNoPost?.isHidden = true
        btnReport?.isHidden = true
        PageView?.isHidden = true
        lblDescription?.isHidden = true;
        
        self.checkReachability()
    }
    
    func checkReachability() {
        
        let manager = NetworkReachabilityManager(host: "www.google.com")
        manager?.startListening()
        
        manager?.listener = {status in
            
            if  manager?.isReachable ?? false {
                
                DispatchQueue.main.async(execute: {
                    print("network connection found")
                    
                    if self.arrayImage1.count > 0 && self.arrayImage2.count > 0 {
                        self.imgNoPost?.isHidden = true
                    }
                    else {
                        self.imgNoPost?.isHidden = false
                    }
                    
                    //                    if self.arrayDict.count >= 0 && self.arrayImage1.count <= 0 && self.arrayImage2.count <= 0 {
                    //                        self.LoadImages()
                    //                        self.isLoadimgImage = true
                    //
                    //                    }
                    
                    if self.isLoadimgImage == false {
                        
                        if self.arrayImage1.count <= 0 && self.arrayImage2.count <= 0 {
                            self.LoadImages()
                        }
                        else {
                            self.AddImageInScroll()
                        }
                    }
                    else {
                        self.AddImageInScroll()
                    }
                    
                    if ((manager?.isReachableOnEthernetOrWiFi) != nil) {
                        //do some stuff
                    }
                    else if(manager?.isReachableOnWWAN)! {
                        //do some stuff
                    }
                })
            }
            else {
                
                DispatchQueue.main.async(execute: {
                    
                    print("no network connection")
                    
                    if self.arrayDict.count <= 0 {
                        self.imgNoPost?.isHidden = false
                    }
                    
                    if self.arrayImage1.count <= 0 && self.arrayImage2.count <= 0 {
                        self.imgNoPost?.isHidden = false
                    }
                    
                    self.isLoadimgImage = false
                    
                    Helper.hideHud(self.view)
                    Helper.hideHud(APPDELEGATE.window!)
                    
                    Helper.openErrorVC(strTitle: "Perfect Attire", isNetworkError: true, parentViewContoller: self, complitionBlock: { (success) in
                        
                        DispatchQueue.main.async(execute: {
                            
                        })
                    })
                })
            }
        }
    }
    
    func getUserList() {
        
        if Helper.checkInternetConnection() == false {
            
            Helper.openErrorVC(strTitle: "Perfect Attire", isNetworkError: true, parentViewContoller: self, complitionBlock: { (success) in
                DispatchQueue.main.async(execute: {
                    self.getUserList()
                })
            })
            
            return
        }
        
        var dictParam = [String:Any]()
        
        dictParam[KPAGENO] = "0"
        if not_user_ids != "" {
            
            not_user_ids.removeLastCharIfExist(",")
            dictParam[KNOTUSERIDS] = not_user_ids
            not_user_ids = ""
        }
        
        APICallManager.sharedInstance.callUploadWebServiceWithParameter(parameters: dictParam, urlPath: ApiCallPath.userList.rawValue, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (apiResult, response) in
            
            if apiResult == ApiResult.success {
                
                let result = response as! [String : AnyObject]
                print("result :- ",result)
                self.arrayDict = result[KUSERLIST] as! [[String: String]]
                
                if self.arrayDict.count > 0 {
                    
                    var  ArrUserId =  self.arrayDict
                    ArrUserId.reverse()
                    
                    for i in (0..<ArrUserId.count)  {
                        self.not_user_ids = self.not_user_ids + (  ArrUserId[i][KUSERID]!) + ","
                        
                        if i  == 1 {
                            break
                        }
                    }
                    
//                    Helper.showHud(self.view)
                    
                    if self.isLoadimgImage == false {
                        self.LoadImages()
                    }
                    //                    self.lblNoImage?.isHidden = true
                    self.imgNoPost?.isHidden = true
                    self.btnReport?.isHidden = false
                    
                    self.PageView?.isHidden = true
                    self.lblDescription?.isHidden = true
                }
                else {
                    //                    self.lblNoImage?.isHidden = false
                    self.imgNoPost?.isHidden = false
                    self.btnReport?.isHidden = true
                    
                    self.PageView?.isHidden = true
                    self.lblDescription?.isHidden = true;
                }
            }
            else if apiResult == ApiResult.error && response != nil {
                print("response :- ",response as! String)
                Helper.hideHud(self.view)
                Helper.showAlertBar(alertBarType: AlertBarType.Error, message: response as! String)
            }
            else {
                Helper.openErrorVC(strTitle: "Perfect Attire", isNetworkError: false, parentViewContoller: self, complitionBlock: { (success) in
                    DispatchQueue.main.async(execute: {
                        self.getUserList()
                    })
                })
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        lblHeader.gradientStartColor = UIColor.init(colorLiteralRed: 120/255.0, green: 187/255.0, blue: 106/255.0, alpha: 1)
        lblHeader.gradientEndColor = UIColor.init(colorLiteralRed: 75/255.0, green: 177/255.0, blue: 173/255.0, alpha: 1)
        lblHeader.gradientStartPoint = CGPoint(x: 0.0, y: 0.0)
        lblHeader.gradientEndPoint = CGPoint(x: 0.0, y: 0.7)
    }
    
    @IBAction func btnTab(sender: UIButton) {
        
        btnEdit?.isHidden = true
        btnBack?.isHidden = true
        
        self.loadUserImage()
        
        if sender.tag == 0 {
            
            btnUser?.tag = 1
            
            lblHeader.text = "Upload"
            
            if APPDELEGATE.isUpload == false {
                lblHeader.text = "Result"
            }
            
            btnReport?.isHidden = true
            btnBottom1?.setImage(UIImage(named:"icn_result_select"), for: .normal)
            btnBottom1?.setTitleColor(UIColor.white, for: .normal)
            btnBottom3?.setTitleColor(UIColor.lightGray, for: .normal)
            btnBottom3?.setImage(UIImage(named:"icn_settings_none_selected"), for: .normal)
            
            self.ScrMain.contentOffset = CGPoint(x: 0, y: 0)
        }
        else if sender.tag == 1 {
            
            btnUser?.tag = 0
            
            if arrayDict.count == 0 {
                self.getUserList()
            }
            
            if arrayImage1.count > 0 &&  arrayImage2.count > 0 {
                btnReport?.isHidden = false
            }
            
            if self.arrayImage1.count <= 0 && self.arrayImage2.count <= 0 {
                if Helper.checkInternetConnection() {
                    if isLoadimgImage == false {
                        self.LoadImages()
                    }
                }
            }
            else {
                AddImageInScroll()
            }
            
            lblHeader.text = "Perfect Attire"
            btnBottom1?.setTitleColor(UIColor.lightGray, for: .normal)
            btnBottom3?.setTitleColor(UIColor.lightGray, for: .normal)
            btnBottom1?.setImage(UIImage(named:"icn_result_none_select"), for: .normal)
            btnBottom3?.setImage(UIImage(named:"icn_settings_none_selected"), for: .normal)
            
            self.ScrMain.contentOffset = CGPoint(x: Int(self.ScrMain.frame.size.width) * 1, y: 0)
        }
        else if sender.tag == 2 || sender.tag == 4 {
            lblHeader.text = "Settings"
            btnReport?.isHidden = true
            btnBottom1?.setTitleColor(UIColor.lightGray, for: .normal)
            btnBottom3?.setTitleColor(UIColor.white, for: .normal)
            btnBottom1?.setImage(UIImage(named:"icn_result_none_select"), for: .normal)
            btnBottom3?.setImage(UIImage(named:"icn_settings_selected"), for: .normal)
            
            self.ScrMain.contentOffset = CGPoint(x: Int(self.ScrMain.frame.size.width) * 2, y: 0)
        }
        
        if sender.tag == 2 || sender.tag == 4 {
            btnUser?.isHidden = true
            if (Nav2?.viewControllers.count)! > 1 {
                Nav2?.popToRootViewController(animated: false)
            }
        }
        else {
            btnUser?.isHidden = false
        }
        
        if APPDELEGATE.isUpload == false {
            btnBottom1?.setTitle("Result", for: UIControlState.normal)
        }
        
    }
    
    
    
    @IBAction func handleTap1(sender: UITapGestureRecognizer) {
        
        if arrayImage1.count > 0 &&  arrayImage2.count > 0 && arrayDict.count > 0 {
            self.imageAction(imageId: self.arrayDict[0]["id\(self.ImageNumber)"]!, userId: self.arrayDict[0]["user_id"]!, actionType: .like)
        }
    }
    
    @IBAction func SaveProfile(_ sender: UIButton) {
        let Setting = (Nav2?.viewControllers[1]) as! ProfileView
        Setting.validateUserData()
    }
    
    func LoadImages() {
        
        if arrayDict.count > 0 {
            
            isLoadimgImage = true
            
            self.PageView?.isHidden = true
            
            Helper.showHud(self.view)
            
            let serialQueue = DispatchQueue(label: "com.queue.Serial")
            
            arrayTemp = arrayDict
            
            for i in 1...arrayTemp.count {
                
                serialQueue.async {
                    
                    if Helper.checkInternetConnection() == false {
                        Helper.hideHud(self.view)
//                        Helper.showAlertBar(alertBarType: AlertBarType.Error, message: "No Internet Connection.")
                        return
                    }
                    
                    let imgURL = URL(string: self.arrayTemp[i-1][KUSERIMAGE1]!)!
                    let imgURL2 = URL(string: self.arrayTemp[i-1][KUSERIMAGE2]!)!
                    
                    var imgData1 : Data = Data()
                    var imgData2 : Data = Data()
                    
                    do {
                        imgData1 = try Data(contentsOf: imgURL)
                        imgData2 = try Data(contentsOf: imgURL2)
                        
                        self.arrayImage1.append(UIImage(data: imgData1)!)
                        print("\(i) Array 1")
                        self.arrayImage2.append(UIImage(data:imgData2)!)
                        print("\(i) Array 2")
                    }
                    catch _{
                        imgData1 = Data()
                        imgData2 = Data()
                    }
                    
                    if i == 1 || self.IsLoading == true {
                        self.AddImageInScroll()
                        self.IsLoading = false
                    }
                    
                    if i == self.arrayTemp.count {
                        self.isLoadimgImage = false
                    }
                }
            }
        }
    }
    
    func AddImageInScroll()  {
        
        DispatchQueue.main.async {
            
            if self.arrayDict.count > 0 {
                if self.arrayImage1.count > 0 && self.arrayImage2.count > 0 {
                    self.lblDescription?.isHidden = false
                    self.lblDescription?.text = self.arrayDict[0][KDESCRIPTION]
                    
                    self.img1.image = self.arrayImage1[0]
                    self.img2.image = self.arrayImage2[0]
                    self.imgBack1.image = self.arrayImage1[0]
                    self.imgBack2.image = self.arrayImage2[0]
                    
                    self.PageView?.isHidden = false
                    self.imgNoPost?.isHidden = true
                    Helper.hideHud(self.view)
                    print("Helper.hideHud :- Add Image in Scroll")
                }
                else {
                    if self.isLoadimgImage == false {
                        self.LoadImages()
                    }
                }
            }
        }
    }
    
    func CheckImage() {
        
        arrayDict.remove(at: 0)
        self.arrayImage1.remove(at: 0)
        self.arrayImage2.remove(at: 0)
        
        if self.arrayImage1.count>0 && self.arrayImage2.count>0 {
            self.AddImageInScroll()
        }
        else {
            
            if arrayDict.count > 0 {
                self.IsLoading = true
                print("Loading...")
                Helper.showHud(self.view)
                
                if isLoadimgImage == false {
                    self.LoadImages()
                }
                
                self.PageView?.isHidden = true
                
                if self.arrayImage1.count <= 0 && self.arrayImage2.count <= 0 {
                    //                    if isLoadimgImage == false {
                    //                        self.LoadImages()
                    //                    }
                }
            } else {
                self.getUserList()
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.tag == 1 {
            if scrollView.contentOffset.x > (self.view.frame.size.width-10) {
                ImageNumber = "1"
                PageRound1?.backgroundColor =  UIColor.lightGray
                PageRound2?.backgroundColor = UIColor(red: 45/255, green: 150/255, blue: 180/255, alpha: 1)
                
            }else{
                ImageNumber = "2"
                
                PageRound1?.backgroundColor = UIColor(red: 45/255, green: 150/255, blue: 180/255, alpha: 1)
                PageRound2?.backgroundColor =  UIColor.lightGray
            }
        }
        
        if scrollView.contentOffset.y != 0 {
            scrollView.contentOffset.y = 0
        }
    }
    
    func imageAction(imageId:String , userId : String , actionType : ImageAction) {
        
        if Helper.checkInternetConnection() == false {
            Helper.openErrorVC(strTitle: "Perfect Attire", isNetworkError: true, parentViewContoller: self, complitionBlock: { (success) in
                DispatchQueue.main.async(execute: {
                    self.imageAction(imageId: imageId, userId: userId, actionType: actionType)
                })
            })
            return
        }
        
        if arrayImage1.count > 0 &&  arrayImage2.count > 0 {
            
            img1.image = nil
            img2.image = nil
            imgBack1.image = nil
            imgBack2.image = nil
            
            lblDescription?.text = ""
            self.PageView?.isHidden = true
            
            self.CheckImage()
        }
        
        ScrView.contentOffset = CGPoint(x: 0, y: 0)
        
        var dictParam = [String:Any]()
        
        dictParam[KIMAGEID] = imageId
        dictParam[KUSERID] = userId
        dictParam[KACTION] = actionType.rawValue
        
        APICallManager.sharedInstance.callWebServiceWithParameter(parameters: dictParam, urlPath: ApiCallPath.imageAction.rawValue, isBackgroundCall: true, pAlertView: self.view, pViewController: self) { (apiResult, response) in
            
            if apiResult == ApiResult.success {
                //                let result = response as! [String : AnyObject]
                //                print("result :- ",result)
                //                Helper.hideHud(self.view)
            }
            else if apiResult == ApiResult.error && response != nil {
                print("response :- ",response as! String)
                //                Helper.hideHud(self.view)
            }
            else {
                ////                Helper.hideHud(self.view)
                //                Helper.openErrorVC(isNetworkError: false, parentViewContoller: self, complitionBlock: { (success) in
                //                    DispatchQueue.main.async(execute: {
                //                        self.imageAction(imageId: imageId, userId: userId, actionType: actionType)
                //                    })
                //                })
            }
        }
    }
    
    @IBAction func btnReport(_ sender: UIButton) {
        
        if arrayDict.count > 0 {
            
            let optionMenu = UIAlertController(title: nil, message: "Report Photo", preferredStyle: .actionSheet)
            
            let Action1 = UIAlertAction(title: "Report Abuse", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
                self.imageAction(imageId: self.arrayDict[0]["id\(self.ImageNumber)"]!, userId: self.arrayDict[0]["user_id"]!, actionType: .abuse)
            })
            
            let Action2 = UIAlertAction(title: "Report Nude", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
                self.imageAction(imageId: self.arrayDict[0]["id\(self.ImageNumber)"]!, userId: self.arrayDict[0]["user_id"]!, actionType: .nude)
                
            })
            
            let Action3 = UIAlertAction(title: "Not for App", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
                self.imageAction(imageId: self.arrayDict[0]["id\(self.ImageNumber)"]!, userId: self.arrayDict[0]["user_id"]!, actionType: .notForApp)
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
            {
                (alert: UIAlertAction!) -> Void in
            })
            
            optionMenu.addAction(Action1)
            optionMenu.addAction(Action2)
            optionMenu.addAction(Action3)
            
            optionMenu.addAction(cancelAction)
            self.present(optionMenu, animated: true, completion: nil)
            
        }
    }
    
}
