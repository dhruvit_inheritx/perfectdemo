//
//  TutorialViewController.swift
//  PerfectDemo
//
//  Created by Dhruvit on 02/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController,UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate {
    
    // Set IBOutlet
    //    @IBOutlet weak var btnNext: UIButton!
    //    @IBOutlet weak var btnPrev: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnGetStarted: UIButton!
    @IBOutlet weak var pageControl: PageControl!
    
    // Set Constants And Variables
    var pageViewController : UIPageViewController!
    var arrPageImages = [kTutorialScreen1, kTutorialScreen2, kTutorialScreen3]
    var Index = 0
    
    //    MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeOnce()
    }
    
    //    MARK:- Custom Methods
    
    func initializeOnce(){
        
        //rotation in radians
        //        btnNext.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        //        btnPrev.isHidden = true
        
        pageViewController = self.storyboard?.instantiateViewController(withIdentifier: kPageViewController) as! UIPageViewController
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        pageControl.activeImage = UIImage.init(named: kIcnPaginationSelected)
        pageControl.inactiveImage = UIImage.init(named: kIcnPaginationNormal)
        
        //PageViewController
        let startingViewController  = viewControllerAtIndex(index: 0)
        let viewcontrollers = [startingViewController]
        pageViewController.setViewControllers(viewcontrollers, direction: .forward, animated: true, completion: nil)
        pageViewController.view.frame.size.height = self.view.frame.size.height + 37
        addChildViewController(pageViewController)
        self.view.addSubview(pageViewController.view)
        pageViewController.didMove(toParentViewController: self)
        
        //        self.view.bringSubview(toFront: btnNext)
        //        self.view.bringSubview(toFront: btnPrev)
        self.view.bringSubview(toFront: btnSkip)
        self.view.bringSubview(toFront: pageControl)
        self.view.bringSubview(toFront: btnGetStarted)
        
        for view in pageViewController.view.subviews {
            if let scrollView = view as? UIScrollView {
                scrollView.delegate = self
            }
        }
    }
    
    func viewControllerAtIndex(index: Int) -> TutorialContentViewController {
        if (self.arrPageImages.count == 0) || (index >= self.arrPageImages.count) {
            return TutorialContentViewController.init()
        }
        // Create a new view controller and pass suitable data.
        let ViewController = self.storyboard!.instantiateViewController(withIdentifier: kTutorialContentViewController) as! TutorialContentViewController
        ViewController.imageFile = self.arrPageImages[index]
        Index = index
        ViewController.pageIndex = index
        
        return ViewController
    }
    
    func setview(index : Int){
        
        if index == 0{
            //            btnPrev.isHidden = true
        }
        else{
            //            btnPrev.isHidden = false
        }
        if index == arrPageImages.count - 1{
            //            btnNext.isHidden = true
            btnSkip.isHidden = true
            btnGetStarted.isHidden = false
        }
        else{
            //            btnNext.isHidden = false
            btnSkip.isHidden = false
            btnGetStarted.isHidden = true
        }
    }
    
    //    MARK:- Action Methods
    
    @IBAction func btnskip(sender: UIButton){
        
        UserDefaults.standard.set(true, forKey: kIsTutorialShown)
        APPDELEGATE.autoLogin()
        
    }
    
    //    @IBAction func btnnext(sender: UIButton){
    //        if (pageControl.currentPage + 1) >= 0 && (pageControl.currentPage + 1) < 3{
    //            self.pageViewController.setViewControllers([viewControllerAtIndex(index: pageControl.currentPage+1)], direction: .forward, animated: false, completion: nil)
    //        }
    //
    //    }
    //
    //    @IBAction func btnprev(sender: UIButton){
    //        if (pageControl.currentPage - 1) >= 0 && (pageControl.currentPage - 1) < 3{
    //            pageViewController.setViewControllers([viewControllerAtIndex(index: pageControl.currentPage-1)], direction: .reverse, animated: false, completion: nil)
    //        }
    //    }
    
    // MARK:- Page View Controller Data Source
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! TutorialContentViewController).pageIndex
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        index -= 1
        return self.viewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! TutorialContentViewController).pageIndex
        if index == NSNotFound {
            return nil
        }
        index += 1
        if index == self.arrPageImages.count {
            return nil
        }
        return self.viewControllerAtIndex(index: index)
        
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int
    {
        return arrPageImages.count
    }
    
    @discardableResult func presentationIndex(for pageViewController: UIPageViewController) -> Int
    {
        if  ((pageViewController.viewControllers?.count) != 0) {
            pageControl.currentPage = (pageViewController.viewControllers![0] as! TutorialContentViewController).pageIndex
            pageControl.updateDots()
            setview(index: pageControl.currentPage)
            return pageControl.currentPage
        }
        return 0
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        presentationIndex(for: pageViewController)
    }
    
    //    MARK:- ScrollView Methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if pageControl.currentPage == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if pageControl.currentPage == arrPageImages.count - 1 && scrollView.contentOffset.x > scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if pageControl.currentPage == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if pageControl.currentPage == arrPageImages.count - 1 && scrollView.contentOffset.x > scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
    }
    
    //    MARK:- Memory Management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
