//
//  TutorialContentViewController.swift
//  PerfectDemo
//
//  Created by Dhruvit on 02/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit

class TutorialContentViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    
    var pageIndex = 0
    var titleText = ""
    var imageFile = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgView.image = UIImage.init(named: imageFile)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated..
    }
    
    convenience init(){
        self.init()
    }

}
