//
//  PageControl.swift
//  PerfectDemo
//
//  Created by Dhruvit on 02/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit

class PageControl: UIPageControl {

    var activeImage: UIImage! = UIImage.init(named: "icn_pagination_selected")
    var inactiveImage: UIImage! = UIImage.init(named: "icn_pagination_normal")

    override var currentPage: Int {
        //willSet {
        didSet { //so updates will take place after page changed
            self.updateDots()
        }
    }
    
    convenience init(activeImage: UIImage, inactiveImage: UIImage) {
        self.init()
        
        self.activeImage = activeImage
        self.inactiveImage = inactiveImage
        
        self.pageIndicatorTintColor = UIColor.clear
        self.currentPageIndicatorTintColor = UIColor.clear
    }
    
    func updateDots() {
        for i in 0 ..< subviews.count {
            let view: UIView = subviews[i]
            if view.subviews.count == 0 {
                self.addImageViewOnDotView(view: view, imageSize: activeImage.size)
            }
            let imageView: UIImageView = view.subviews.first as! UIImageView
            imageView.image = self.currentPage == i ? activeImage : inactiveImage
        }
    }
    
    // MARK: - Private
    
    func addImageViewOnDotView(view: UIView, imageSize: CGSize) {
        var frame = view.frame
        frame.origin = .zero
        frame.size = imageSize
        
        let imageView = UIImageView(frame: frame)
        imageView.contentMode = UIViewContentMode.center
        view.addSubview(imageView)
    }

}
