    //
//  ErrorView.swift
//  PerfectDemo
//
//  Created by Kuldeep Gadhvi on 17/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit




class ErrorView: UIViewController {

    var onHideComplete: ((_ success : String) -> Void)?
    
    @IBOutlet weak var imgView : UIImageView?
    @IBOutlet weak var lblTitle : UILabel?
    
    var IsNetworkError = true
    var strTitle : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle?.text = strTitle
        
        if IsNetworkError == true {
            imgView?.image = UIImage(named: "no_internet")
        }else{
            imgView?.image = UIImage(named: "opps_error")
        }
        
        
        self.navigationController?.present(self, animated: true, completion: { 
            
        })
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnBack(_ sender: UIButton) {
    self.dismiss(animated: false)
    }
    
    @IBAction func btnRetry(_ sender: UIButton) {
        
        if IsNetworkError == true {
            if Helper.checkInternetConnection() == false {
                return
            }
        }
        
        if let callback = self.onHideComplete {
            callback ("Retry")
            self.dismiss(animated: false)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
