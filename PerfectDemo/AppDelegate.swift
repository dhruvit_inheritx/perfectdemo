//
//  AppDelegate.swift
//  PerfectDemo
//
//  Created by Dhruvit on 01/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit
import CoreLocation
import FBSDKCoreKit
import FBSDKLoginKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var locationManager = CLLocationManager()
    var timer : Timer!
    
    var isUpload : Bool = true
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        sleep(2)
        
        IQKeyboardManager.sharedManager().enable = true
        
        self.requestLocationPermission()
        
//        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //        USERDEFAULTS.setValue(false, forKey: kIsTutorialShown)
        //        USERDEFAULTS.setValue(false, forKey: kISUSERLOGGEDIN)
        
        self.autoLogin()
        
        return true
    }
    
    func updateLocation() {
        
        if USERDEFAULTS.bool(forKey: kISUSERLOGGEDIN) == true {
            
            guard let strLatitude = USERDEFAULTS.object(forKey: KLATITUDE) else {
                return
            }
            
            guard let strLongitude = USERDEFAULTS.object(forKey: KLONGITUDE) else {
                return
            }
            
            var dictLatLong = [String:Any]()
            dictLatLong[KLATITUDE] = strLatitude
            dictLatLong[KLONGITUDE] = strLongitude
            
            APICallManager.sharedInstance.callWebServiceWithParameter(parameters: dictLatLong, urlPath: "update-user-lat-long", isBackgroundCall: true, pAlertView: self.window, pViewController: self.window?.parentViewController, pCompletionBlock: { (apiResult, response) in
                
                if apiResult == ApiResult.success {
                    print("User Location updated successfully.")
                }
                else {
                    print("Error :- ",response as! String)
                }
                
            })
            
        }
    }
    
    func autoLogin(){
        
        UIApplication.shared.isStatusBarHidden = true
        
        if USERDEFAULTS.bool(forKey: kIsTutorialShown) != true {
            
            let objStoryboard: UIStoryboard = UIStoryboard(name: sTutorialScreens, bundle: Bundle.main)
            let objVC: UIViewController = objStoryboard.instantiateViewController(withIdentifier: kTutorialViewController)
            self.window!.rootViewController = objVC
            self.window!.makeKeyAndVisible()
            
        }
        else {
            if USERDEFAULTS.bool(forKey: kISUSERLOGGEDIN) == true {
                UIApplication.shared.isStatusBarHidden = true
                let objStoryboard: UIStoryboard = UIStoryboard(name: sMain, bundle: Bundle.main)
                let objVC: UIViewController = objStoryboard.instantiateInitialViewController()!
                APPDELEGATE.window!.rootViewController = objVC
                APPDELEGATE.window!.makeKeyAndVisible()
            }
            else {
                UIApplication.shared.isStatusBarHidden = true
                let objStoryboard: UIStoryboard = UIStoryboard(name: sAuthentication, bundle: Bundle.main)
                let objVC: UIViewController = objStoryboard.instantiateInitialViewController()!
                APPDELEGATE.window!.rootViewController = objVC
                APPDELEGATE.window!.makeKeyAndVisible()
            }
        }
    }
    
    func Logout() {
        
        APPDELEGATE.isUpload = true
        
        FBSDKLoginManager.init().logOut()
        
        USERDEFAULTS.set(false, forKey: kISUSERLOGGEDIN)
        USERDEFAULTS.removeObject(forKey: KACCESSTOKEN)
        USERDEFAULTS.removeObject(forKey: KID)
        USERDEFAULTS.removeObject(forKey: KPROFILEPICTURE)
        USERDEFAULTS.removeObject(forKey: KLATITUDE)
        USERDEFAULTS.removeObject(forKey: KLONGITUDE)
        
        self.autoLogin()
        
        //        let objStoryboard: UIStoryboard = UIStoryboard(name: sAuthentication, bundle: Bundle.main)
        //        let objVC: UIViewController = objStoryboard.instantiateViewController(withIdentifier: kAuthenticationNav)
        //        self.window!.rootViewController = objVC
        //        self.window!.makeKeyAndVisible()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let characterSet: CharacterSet = CharacterSet(charactersIn: "<>")
        
        let deviceTokenString: String = (deviceToken.description as NSString)
            .trimmingCharacters(in: characterSet)
            .replacingOccurrences( of: " ", with: "") as String
        UserDefaults.standard.set(deviceTokenString, forKey: KDEVICETOKEN)
        UserDefaults.standard.synchronize()
        
        print("DEVICETOKEN: \(deviceTokenString)")
        NSLog("DEVICETOKEN: \(deviceTokenString)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        if error._code == 3010 {
            print("Push notifications are not supported in the iOS Simulator.", terminator: "")
        } else {
            print("application:didFailToRegisterForRemoteNotificationsWithError: %@", error, terminator: "")
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func requestLocationPermission() {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if #available(iOS 9.0, *) {
            locationManager.requestLocation()
        } else {
            // Fallback on earlier versions
        }
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        
        self.checkLocationPermission()
    }
    
    func checkLocationPermission() {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                
                Helper.showAlertwithTitle("Location Services Disabled", strMessage: "To re-enable, please go to Settings and turn on Location Service for this app.", pobjView: self.window)
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
            Helper.showAlertwithTitle("Location Services Disabled", strMessage: "To re-enable, please go to Settings and turn on Location Service for this app.", pobjView: self.window)
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        
    }
    
    // MARK: - Other methods
    func getVersionString() -> String{
        let nsObject: AnyObject? = Bundle.main.infoDictionary![BUNDLESHORTVERSION] as AnyObject?
        let version = nsObject as! String
        let strVersion = "v " + version + BUILD
        return strVersion
    }
    
}

extension AppDelegate : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("User allowed us to access location")
            //do whatever init activities here.
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userCoordinate : CLLocationCoordinate2D = locations.last!.coordinate
        
        USERDEFAULTS.setValue(userCoordinate.latitude, forKey: KLATITUDE)
        USERDEFAULTS.setValue(userCoordinate.longitude, forKey: KLONGITUDE)
        
        //        print("latitude :- ",userCoordinate.latitude)
        //        print("longitude :- ",userCoordinate.longitude)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("didFailWithError")
        
//        AlertBar.show(type: AlertBarType.Info, message: "Failed to fetch User Location.", parentView: self.window!)
    }
    
}
