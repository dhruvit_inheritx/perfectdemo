//
//  CommonView.swift
//  PerfectDemo
//
//  Created by Kuldeep Gadhvi on 26/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit

class CommonView: UIViewController {
    
    var index = 0
    @IBOutlet weak var lblVersion : UILabel?
    @IBOutlet weak var lblAppName : UILabel?
    @IBOutlet weak var ImageLogo : UIImageView?
    @IBOutlet weak var viewVersion : UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        lblVersion?.text = "Version : " + appVersionString
        
         let iconsDictionary = Bundle.main.infoDictionary?["CFBundleIcons"] as? NSDictionary
            let primaryIconsDictionary = iconsDictionary?["CFBundlePrimaryIcon"] as? NSDictionary
            let iconFiles = primaryIconsDictionary?["CFBundleIconFiles"] as? NSArray
        ImageLogo?.image = UIImage(named:iconFiles!.lastObject as! String)
        
        let infoDictionary: NSDictionary = Bundle.main.infoDictionary as NSDictionary!
        let appName: NSString = infoDictionary.object(forKey: "CFBundleDisplayName") as! NSString
        
        lblAppName?.text = appName as String
        
        if index == 4 {
            viewVersion.isHidden = false
        }
        else {
            viewVersion.isHidden = true
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
