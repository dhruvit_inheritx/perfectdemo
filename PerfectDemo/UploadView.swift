//
//  UploadView.swift
//  PerfectDemo
//
//  Created by Dhruvit on 10/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit

class UploadView: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var ImageUploaded: ((_ success : String) -> Void)?
    
//    @IBOutlet var imgView : UIImageView!
    @IBOutlet var btnUser : UIButton!
    @IBOutlet var imgView1 : UIImageView!
    @IBOutlet var imgView2 : UIImageView!
    @IBOutlet var txtDesc : UITextView!
    @IBOutlet var btnUpload : UIButton!
    @IBOutlet var lblCharCount : UILabel!
    
    @IBOutlet var viewDesc : UIView!
    @IBOutlet var lblUnderDev : UILabel!
    
    var imgSelected : UIImageView!
    
    var intSelectedImageView : Int = 0
    var img1 : UIImage!
    var img2 : UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblCharCount.text = "0 / 100"
        
        // Do any additional setup after loading the view.
        
        txtDesc.textColor = UIColor.lightGray
        
        imgSelected = UIImageView()
        
        lblUnderDev.isHidden = true
    }
    
    @IBAction func btnUserClicked(sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func handleTap1(sender: UITapGestureRecognizer) {
        
        print(sender.view?.tag ?? 0)
        
        imgSelected = sender.view  as! UIImageView
        intSelectedImageView = (sender.view?.tag)!
        
        let alert = UIAlertController(title: "Select Image From", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imgPicker = UIImagePickerController()
                imgPicker.sourceType = .photoLibrary
                imgPicker.delegate = self
                imgPicker.allowsEditing = false
                DispatchQueue.main.async {
                    self.present(imgPicker, animated: true, completion: nil)
                }
            }
            else {
                AlertBar.show(type: AlertBarType.Warning, message: "Photo Library not Available.", parentView: self.view)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imgPicker = UIImagePickerController()
                imgPicker.sourceType = .camera
                imgPicker.delegate = self
                imgPicker.allowsEditing = false
                DispatchQueue.main.async {
                    self.present(imgPicker, animated: true, completion: nil)
                }
            }
            else {
                AlertBar.show(type: AlertBarType.Warning, message: "Camera not Available.", parentView: self.view)
            }

        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnUploadPhotos(_ sender: UIButton) {
        
        if img1 == nil || img2 == nil {
            Helper.showAlertBar(alertBarType: AlertBarType.Warning, message: "Please Select Images First.")
            return
        }
        
        if txtDesc.text.characters.count > 100 {
            Helper.showAlertBar(alertBarType: AlertBarType.Warning, message: "Description must be less than 100 charactars long.")
            return
        }
        
//        UIView.animate(withDuration: 1,
//                       animations: {
//                        sender.transform = CGAffineTransform(scaleX: 1.6, y: 1.6)
//                        sender.alpha = 0
//        },
//                       completion: { _ in
        sender.isHidden = true
        
        if Helper.checkInternetConnection() == false {
            Helper.openErrorVC(strTitle: "Upload", isNetworkError: true, parentViewContoller: self, complitionBlock: { (success) in
                DispatchQueue.main.async(execute: {
                    self.btnUploadPhotos(UIButton())
                })
            })
            return
        }
        
        self.uploadImageSet()
//        })

        
        
        
//        self.uploadImageSet()
        
    }
    
    func uploadImageSet() {
        
        var dictParam = [String:Any]()
        
        if txtDesc.text == "Description" {
            dictParam[KDESCRIPTION] = ""
        }
        else {
            dictParam[KDESCRIPTION] = txtDesc.text
        }        
        
        var dictUpload = [String:Data]()
        
        let imgData1 = UIImageJPEGRepresentation(img1, 0.3)
        let imgData2 = UIImageJPEGRepresentation(img2, 0.3)
        
        dictUpload[KUSERIMAGE1] = imgData1
        dictUpload[KUSERIMAGE2] = imgData2

        APICallManager.sharedInstance.callUploadWebServiceWithParameter(uploadData : dictUpload ,parameters: dictParam, urlPath: ApiCallPath.imageUpload.rawValue, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (apiResult, response) in
            
            if apiResult == ApiResult.success {
                Helper.showAlertBar(alertBarType: AlertBarType.Success, message: "Uploaded Successfully.")
                self.btnUpload.isHidden = false
                
                APPDELEGATE.isUpload = false
                
                if let callback = self.ImageUploaded {
                    callback ("1")
                }
                
                self.imgView1.isHidden = true
                self.imgView2.isHidden = true
                self.txtDesc.isHidden = true
                self.viewDesc.isHidden = true
                self.lblCharCount.isHidden = true
                self.lblUnderDev.isHidden = false
                self.btnUpload.isUserInteractionEnabled = false
                self.btnUpload.isHidden = true
            }
            else if apiResult == ApiResult.error && response != nil {
                self.btnUpload.isHidden = false
                print("response :- ",response as! String)
                Helper.hideHud(self.view)
                Helper.showAlertBar(alertBarType: AlertBarType.Error, message: response as! String)
                
                self.btnUpload.isHidden = false
                self.btnUpload.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.btnUpload.alpha = 1.0
            }
            else {
                self.btnUpload.isHidden = false
                self.btnUpload.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.btnUpload.alpha = 1.0

                Helper.openErrorVC(strTitle: "Upload", isNetworkError: false, parentViewContoller: self, complitionBlock: { (success) in
                    DispatchQueue.main.async(execute: {
                        self.uploadImageSet()
                    })
                })
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            imgSelected.image = selectedImage
        
        if intSelectedImageView == 1 {
            img1 = selectedImage
        }
        else if intSelectedImageView == 2 {
            img2 = selectedImage
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension UploadView : UITextViewDelegate {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        OperationQueue.main.addOperation { 
            UIMenuController.shared.setMenuVisible(false, animated: false)
        }
        
        return false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
//        print(text)
        let strFull = txtDesc.text + text
        
        lblCharCount.text = "\(strFull.characters.count) / 100"
        
        if text == "" {
            if textView.text != "" {
                lblCharCount.text = "\(strFull.characters.count - 1) / 100"
            }
            return true
        }
        
        if txtDesc.text.length >= 100 {
            lblCharCount.text = "\(strFull.characters.count - 1) / 100"
            
            if text == "\n" {
                print("text = \n ")
                if textView.text != "" {
                    lblCharCount.text = "\(strFull.characters.count - 1) / 100"
                }
                self.view.endEditing(true)
                return false
            }
            
            return false
        }
        
        if text == "\n" {
            print("text = \n ")
            if textView.text != "" {
                lblCharCount.text = "\(strFull.characters.count - 1) / 100"
            }
            self.view.endEditing(true)
            return false
        }
        else {
            print(text)
            return true
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        lblCharCount.text = "\(textView.text.length) / 100"
        
        if textView.text.isEmpty {
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        }
    }
    
}
