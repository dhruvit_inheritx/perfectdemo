//
//  Helper.swift
//  PerfectDemo
//
//  Created by Dhruvit on 02/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

typealias alertComplitionBlock = (Any, NSInteger) -> ()
typealias errorVCComplitionBlock = (String) -> ()

class Helper: NSObject {
    
    static func showAlertBar(alertBarType : AlertBarType , message : String , parentView : UIView = APPDELEGATE.window!) {
        
        DispatchQueue.main.async { 
            AlertBar.show(type: alertBarType, message: message, parentView: parentView)
        }
    }
    
    static func saveUserDetails(dictDetail : [String : Any]) {
        
        let dictUserDetail = dictDetail[KUSERDETAILS] as! [String:Any]
        let objUserDetails = ClsUserDetails(pdictResponse: dictUserDetail)
        
        objUserDetails.downloadProfilePic()
        
        if let strAccessToken = objUserDetails.strAccessToken
        {
            APICallRouter.AccessToken = strAccessToken
            USERDEFAULTS.set(strAccessToken, forKey: KACCESSTOKEN)
        }
        
        if let strUserID = objUserDetails.strId
        {
            APICallRouter.UserID = strUserID
            USERDEFAULTS.set(strUserID, forKey: KID)
        }
        
        USERDEFAULTS.setUserDetailsObject(objUserDetails)
        USERDEFAULTS.set(true, forKey: kISUSERLOGGEDIN)
        USERDEFAULTS.synchronize()
    }
    
    static func openErrorVC(strTitle : String ,isNetworkError:Bool,parentViewContoller : UIViewController , complitionBlock : @escaping errorVCComplitionBlock) {
        let errorVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ErrorView") as! ErrorView
        errorVC.IsNetworkError = isNetworkError
        errorVC.strTitle = strTitle
        errorVC.onHideComplete = {
            success in
            if success == "Retry"{
                complitionBlock(success)
            }
        }
        parentViewContoller.navigationController?.present(errorVC, animated: false)
    }
    
    static func printAllFonts() {
        //        Print all fonts in swift 3.0
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName)
            print("Font Names = [\(names)]")
        }
    }
    
    func getVersionString() -> String {
        let nsObject: AnyObject? = Bundle.main.infoDictionary![BUNDLESHORTVERSION] as AnyObject?
        let version = nsObject as! String
        let strVersion = "v " + version + BUILD
        return strVersion
    }
    
    static func checkInternetConnection() -> Bool {
        if APICallManager.sharedInstance.isConnectedToNetwork() == true {
            return true
        }
        else {
            return false
        }
    }
    
    static func showAlert(_ strMessage:String, pobjView:UIView!){
        DispatchQueue.main.async { () -> Void in
            
            let alertView = CustomAlert(title: APPNAME, message:strMessage.uppercaseFirst, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNOKTITLE, otherButtonTitle: nil) { (customAlert, buttonIndex) -> Void in
                
            }
            alertView?.show(in: pobjView)
        }
    }
    
    static func showAlert(_ strMessage:String, pobjView:UIView!,completionBock : @escaping alertComplitionBlock){
        
        let alertView = CustomAlert(title: APPNAME, message:strMessage.uppercaseFirst, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNOKTITLE, otherButtonTitle: nil) { (customAlert, buttonIndex) -> Void in
            completionBock(customAlert!, buttonIndex)
        }
        alertView?.show(in: pobjView)
    }
    
    static func showAlert(_ strMessage:String, cancelButtonTitle:String,otherButtonTitle : String, pobjView:UIView!,completionBock : @escaping alertComplitionBlock){
        
        let alertView = CustomAlert(title: APPNAME, message:strMessage, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: cancelButtonTitle, otherButtonTitle: otherButtonTitle) { (customAlert, buttonIndex) -> Void in
            completionBock(customAlert!, buttonIndex)
        }
        alertView?.show(in: pobjView)
    }
    static func showAlertwithTitle(_ strTitle:String , strMessage:String , pobjView:UIView!,completionBock : @escaping alertComplitionBlock){
        let alertView = CustomAlert(title: strTitle, message:strMessage.uppercaseFirst, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNOKTITLE, otherButtonTitle: nil) { (customAlert, buttonIndex) -> Void in
            completionBock(customAlert!, buttonIndex)
        }
        alertView?.show(in: pobjView)
    }
    
    static func showAlertwithTitle(_ strTitle:String , strMessage:String , cancelButtonTitle:String,otherButtonTitle : String, pobjView:UIView!,completionBock : @escaping alertComplitionBlock){
        let alertView = CustomAlert(title: strTitle, message:strMessage.uppercaseFirst, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: cancelButtonTitle, otherButtonTitle: otherButtonTitle) { (customAlert, buttonIndex) -> Void in
            completionBock(customAlert!, buttonIndex)
        }
        alertView?.show(in: pobjView)
    }
    
    static func showAlertwithTitle(_ strTitle:String , strMessage:String, pobjView:UIView!){
        
        let alertView = CustomAlert(title: strTitle, message:strMessage.uppercaseFirst, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNOKTITLE, otherButtonTitle: nil) { (customAlert, buttonIndex) -> Void in
            
        }
        alertView?.show(in: pobjView)
    }
    
    static func showNetworkIndicator(){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    static func hideNetworkIndicator(){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    static func showHud(_ pview : UIView){
        
        DispatchQueue.main.async { () -> Void in
            MBProgressHUD.showAdded(to: pview, animated: true)
        }
    }
    
    static func hideHud(_ pview : UIView){
        
        DispatchQueue.main.async { () -> Void in
            MBProgressHUD.hide(for: pview, animated: true)
        }
    }
    
    //MARK: - UserDefaults Methods
    
    static func addToUserDefaults(pobject:AnyObject,forKey:String) {
        
        UserDefaults.standard.set(pobject, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    static func getFromUserDefaults(forKey:String)-> AnyObject {
        
        let object = UserDefaults.standard.object(forKey: forKey)
        return object as AnyObject;
    }
    
    static func removeFromUserDefaults(forKey:String) {
        
        UserDefaults.standard.removeObject(forKey: forKey)
        UserDefaults.standard.synchronize()
    }
   
    static func formatNumber(mobileNumber: String) -> String {
        var number : String!
        number = mobileNumber.replacingOccurrences(of: "(", with: "")
        number = number.replacingOccurrences(of: ")", with: "")
        number = number.replacingOccurrences(of: " ", with: "")
        number = number.replacingOccurrences(of: "-", with: "")
        number = number.replacingOccurrences(of: "+", with: "")
        let length: Int = Int(number.characters.count)
        if length > 10 {
            number = mobileNumber[0..<length-10]
        }
        return number
    }
    
    static func getLength(mobileNumber: String) -> Int {
        var number : String!
        number = mobileNumber.replacingOccurrences(of: "(", with: "")
        number = number.replacingOccurrences(of: ")", with: "")
        number = number.replacingOccurrences(of: " ", with: "")
        number = number.replacingOccurrences(of: "-", with: "")
        number = number.replacingOccurrences(of: "+", with: "")
        let length: Int = Int(number.characters.count)
        return length
    }
    
    static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth,height: newWidth))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newWidth))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}
