import Foundation
import UIKit


// MARK: - iPhone Model

let IS_IPHONE_4_OR_LESS = UIScreen .main.bounds.size.height < 568.0
let IS_IPHONE_5 = UIScreen .main.bounds.size.height == 568.0
let IS_IPHONE_6 = UIScreen .main.bounds.size.height == 667.0
let IS_IPHONE_6P = UIScreen .main.bounds.size.height == 736.0
let IS_OS_8_OR_LATER = (UIDevice.current.systemVersion as NSString).floatValue >= 8.0

// MARK: - Application

let BUNDLESHORTVERSION = "CFBundleShortVersionString"

let KSCREENWIDTH = UIScreen.main.bounds.width
let KSCREENHEIGHT = UIScreen.main.bounds.height

let APPDELEGATE:AppDelegate =  UIApplication.shared.delegate as! AppDelegate
let cApplication = "PerfectDemo"

let BUILD = "1"

// UserDefault
let USERDEFAULTS = UserDefaults.standard

// NotificationCenter
let NOTIFICATIONCENTER = NotificationCenter.default

//API Spec

let APIKEY = "ba1f0ea830c460cc23c8c195196ced76"
let APISECRET = "secret"

let KBASEURL = "http://inheritxdev.net/PerfectAttire_dev/code/web/api/v1/api/"

// MARK: - Constants Segue

let cSegueMainStoryboard = "segueMainStoryboard"
let cSegueTermsCondition = "segueTerms&Condition"
let cSeguePrivacyPolicy = "seguePrivacyPolicy"

// MARK: - Constants StoryBoard

let sMain = "Main"
let sAuthentication = "Authentication"
let sTutorialScreens = "TutorialScreens"

let kAuthenticationNav = "AuthenticationNav"

// MARK: - ViewControllers

let kTutorialViewController = "TutorialViewController"

// MARK: - Web Services

let kBaseUrl = "http://www.inheritxdev.net/Social_App/api/web"

// MARK: - Tutorial Screens

let kTutorialScreen1 = "tutorial_screen_1"
let kTutorialScreen2 = "tutorial_screen_2"
let kTutorialScreen3 = "tutorial_screen_3"
let kPageViewController = "PageViewController"
let kTutorialContentViewController = "TutorialContentViewController"
let kIcnPaginationSelected = "icn_pagination_selected"
let kIcnPaginationNormal = "icn_pagination_normal"

// MARK: - Bool

let kISUSERLOGGEDIN = "isLogged"
let kIsTutorialShown = "isTutorialScreenShow"


// MARK: - ErrorCode

let SESSIONEXPIRED = 401
let REQUESTSUCCESS = 100
let REQUESTRESULTNOTFOUND = 400

// MARK: - Parameters

let KCODE = "code"
let KNONCE = "nonce"
let KTOKEN = "token"
let KSOURCE = "source_name"
let KDEVICETOKEN = "device_token"

let KLATITUDE = "latitude"
let KLONGITUDE = "longitude"

let KERROR = "error"
let KMESSAGE = "message"
let KDATA = "data"

// UserResult

let KID1 = "id1"
let KID2 = "id2"
let KUSERIMAGE1 = "user_image1"
let KUSERIMAGE2 = "user_image2"
let KTOTALLIKE1 = "total_like1"
let KTOTALLIKE2 = "total_like2"
let KDESCRIPTION = "description"

// Get UserImageSet List

let KPAGENO = "page_no"
let KCOUNT = "count"
let KNOTUSERIDS = "not_user_ids"

let KUSERRESULT = "userResult"
let KUSERLIST = "user_list"
let KUSERDETAILS = "userDetails"

// Image Action

let KACTION = "action"
let KUSERID = "user_id"
let KIMAGEID = "image_id"

// UserDetail Constants

let EMPTYSTRING = ""
let KID = "id"
let KFBID = "fb_id"
let KEMAIL = "email"
let KFBFIRSTNAME = "first_name"
let KFBLASTNAME = "last_name"
let KGENDER = "gender"
let KCONTACTNO = "contact_no"
let KFBPICTURE = "picture"
let KFBDATA = "data"
let KFBURL = "url"
let KPROFILEPICTURE = "profile_pic"
let KACCESSTOKEN = "access_token"
//let KFIRSTNAME = "firstname"
//let KLASTNAME = "lastname"
//let KFULLNAME = "fullname"
let KDOB = "dob"


// Color

let kThemeMainLabelColor = UIColor(red: 120/255, green: 187/255, blue: 106/255, alpha: 1)
let kThemeDestructiveColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1)
let kThemeColor = UIColor(red: 120/255, green: 187/255, blue: 106/255, alpha: 1)

// Message
let APPNAME = "Perfect Attire"
let ALERTBLANKFNAME = "Please enter firstname."
let ALERTBLANKLNAME = "Please enter lastname."
let ALERTBLANKUSERNAME = "Please enter username."
let ALERTBLANKPHONENUMBER = "Please enter mobile number."
let ALERTBLANKEMAILADDRESS = "Please enter email address."
let ALERTBLANKDOB = "Please enter Date of Birth."

// Button title
let BTNOKTITLE = "OK"
let BTNRESETTITLE = "Reset"
let BTNREQUESTTITLE = "Request"
let BTNCANCELTITLE = "Cancel"
let BTNYESTITLE = "Yes"
let BTNNOTITLE = "No"


let STREMPTY = ""
let EMAILREGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
let PHONECHARS = "+0123456789"

let  CGFLOATZERO : CGFloat = 0.0
let FLOATZERO : Float = 0.0
let INTZERO : Int = 0

